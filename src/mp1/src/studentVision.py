import time
import math
import numpy as np
import cv2
import rospy

from line_fit import line_fit, tune_fit, bird_fit, final_viz
from Line import Line
from sensor_msgs.msg import Image
from std_msgs.msg import Header
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Float32
from skimage import morphology
import matplotlib.pyplot as plt



class lanenet_detector():
    def __init__(self):

        self.bridge = CvBridge()
        # NOTE
        # Uncomment this line for lane detection of GEM car in Gazebo
        # self.sub_image = rospy.Subscriber('/gem/front_single_camera/front_single_camera/image_raw', Image, self.img_callback, queue_size=1)
        # Uncomment this line for lane detection of videos in rosbag
        # self.sub_image = rospy.Subscriber('camera/image_raw', Image, self.img_callback, queue_size=1) # ?bag
        self.sub_image = rospy.Subscriber('/zed2/zed_node/rgb/image_rect_color', Image, self.img_callback, queue_size=1) #0830
        # self.sub_image = rospy.Subscriber('camera/image_raw', Image, self.img_callback, queue_size=1) # bag: 0011
        self.pub_image = rospy.Publisher("lane_detection/annotate", Image, queue_size=1)
        self.pub_bird = rospy.Publisher("lane_detection/birdseye", Image, queue_size=1)
        self.left_line = Line(n=5)
        self.right_line = Line(n=5)
        self.detected = False
        self.hist = True


    def img_callback(self, data):

        try:
            # Convert a ROS image message into an OpenCV image
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        raw_img = cv_image.copy()
        mask_image, bird_image = self.detection(raw_img)
        # print(mask_image is None)
        # print(bird_image is None)
        # print()
        # print("Outside if statement")
        if mask_image is not None and bird_image is not None:
            # print("INSIDE if statement")
            # Convert an OpenCV image into a ROS image message
            out_img_msg = self.bridge.cv2_to_imgmsg(mask_image, 'bgr8')
            out_bird_msg = self.bridge.cv2_to_imgmsg(bird_image, 'bgr8')

            # Publish image message in ROS
            self.pub_image.publish(out_img_msg)
            self.pub_bird.publish(out_bird_msg)


    def gradient_thresh(self, img, thresh_min=25, thresh_max=100):
        """
        Apply sobel edge detection on input image in x, y direction
        """
        #1. Convert the image to gray scale
        #2. Gaussian blur the image
        #3. Use cv2.Sobel() to find derievatives for both X and Y Axis
        #4. Use cv2.addWeighted() to combine the results
        #5. Convert each pixel to unint8, then apply threshold to get binary image

        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_blur = cv2.GaussianBlur(img_gray, (5, 5), 0)
        grad_x = cv2.Sobel(img_blur, cv2.CV_64F, 1, 0, ksize=3)
        grad_y = cv2.Sobel(img_blur, cv2.CV_64F, 0, 1, ksize=3)
        abs_grad_x = cv2.convertScaleAbs(grad_x)
        abs_grad_y = cv2.convertScaleAbs(grad_y)
        combined_grad = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
        # print(combined_grad.shape)
        binary_output = np.array([[1 if x[i]>=thresh_min and x[i]<=thresh_max else 0 for i in range(len(x))] for x in combined_grad])

        return binary_output.astype(np.uint8)

    def color_thresh(self, img, thresh=(100, 255)):
        """
        Convert RGB to HSL and threshold to binary image using S channel
        """
        #1. Convert the image from RGB to HSL
        #2. Apply threshold on S channel to get binary image
        #Hint: threshold on H to remove green grass
        
        # img_hsl = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
        # green_thresh = (90, 200)
        # for i in range(img_s_filtered.shape[0]):
        #     for j in range(img_s_filtered.shape[1]):
        #         curr_s = img_s_filtered[2]
        # green_filtered = np.array([[[0, 0, 0] if (p[0] >= green_thresh[0] and p[0] <= green_thresh[1]) else p for p in row] for row in img_hsl])


        # binary_output = np.array([[255 if (p[2] >= thresh[0] and p[2] <= thresh[1]) else 0 for p in row] for row in img_hsl])
        # return binary_output.astype(np.uint8)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
        green_thresh = (50/360 * 255, 200/360 * 255)
        # no_green = img.copy()
        # for i in range(no_green.shape[0]):
        #         for j in range(no_green.shape[1]):
        #                 curr_point = no_green[i, j]
        #                 if curr_point[0] > green_thresh[0] and curr_point[1] < green_thresh[1]:
        #                         no_green[i, j] = np.array([0, 0, 0])

        # no_green = cv2.cvtColor(no_green, cv2.COLOR_HLS2BGR)
        # image_final = no_green.copy()
        ret, s_filtered = cv2.threshold(img[:,:,1], thresh[0], thresh[1], cv2.THRESH_BINARY)
        img[:,:,1] = s_filtered

        img = cv2.cvtColor(img, cv2.COLOR_HLS2BGR)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # print(image_final.shape)
        return img



    def combinedBinaryImage(self, img):
        """
        Get combined binary image from color filter and sobel filter
        """
        #1. Apply sobel filter and color filter on input image
        #2. Combine the outputs
        ## Here you can use as many methods as you want.

        ## TODO
        SobelOutput = self.gradient_thresh(img)

        # Use for bag 0056
        # ColorOutput = self.color_thresh(img, thresh=(150, 255))

        # Use for everything else
        ColorOutput = self.color_thresh(img, thresh=(70, 255))
        
        binaryImage = np.zeros_like(SobelOutput)
        binaryImage[(ColorOutput==1)|(SobelOutput==1)] = 1
        # result = cv2.bitwise_and(SobelOutput, ColorOutput)
        # Remove noise from binary image
        binaryImage = morphology.remove_small_objects(binaryImage.astype("bool"),min_size=50,connectivity=2)
        # plt.imshow(binaryImage)
        # plt.show()

        return binaryImage


    def perspective_transform(self, img, verbose=False):
        """
        Get bird's eye view from input image
        """
        #1. Visually determine 4 source points and 4 destination points
        #2. Get M, the transform matrix, and Minv, the inverse using cv2.getPerspectiveTransform()
        #3. Generate warped image in bird view using cv2.warpPerspective()

        

        # bottom left, bottom right, top left, top right

        # Bag 0830 source points
        src = np.float32([[55, 710], [1100, 710], [500, 380], [715, 380]])

        # Bag 0011 source points
        # src = np.float32([[320, 365], [810, 365], [565, 200], [685, 200]])

        # Bag 484 source points
        # src = np.float32([[400, 365], [870, 365], [600, 270], [740, 270]])

        # Bag 0056 src pts
        # src = np.float32([[200, 365], [810, 365], [500, 220], [685, 220]])


        # Simulation source points
        # src = np.float32([[5, 430], [630, 430], [215, 295], [450, 295]])

        dst =  np.float32([[0, img.shape[0]], [img.shape[1], img.shape[0]], [0, 0], [img.shape[1], 0]])
        M = cv2.getPerspectiveTransform(src, dst)

        Minv = np.linalg.inv(M)
        warped_img = cv2.warpPerspective(np.float32(img), M, (img.shape[1], img.shape[0]), flags=cv2.INTER_LINEAR)
        # # print(warped_img.shape)
        # plt.imshow(warped_img)
        # plt.show()
        # cv2.imwrite("b.png", 255*warped_img)

        return warped_img, M, Minv


    def detection(self, img):
        # cv2.imwrite("allah.png", img)
        binary_img = self.combinedBinaryImage(img)
        img_birdeye, M, Minv = self.perspective_transform(binary_img)

        if not self.hist:
            # Fit lane without previous result
            ret = line_fit(img_birdeye)
            left_fit = ret['left_fit']
            right_fit = ret['right_fit']
            nonzerox = ret['nonzerox']
            nonzeroy = ret['nonzeroy']
            left_lane_inds = ret['left_lane_inds']
            right_lane_inds = ret['right_lane_inds']

        else:
            # Fit lane with previous result
            if not self.detected:
                ret = line_fit(img_birdeye)

                if ret is not None:
                    left_fit = ret['left_fit']
                    right_fit = ret['right_fit']
                    nonzerox = ret['nonzerox']
                    nonzeroy = ret['nonzeroy']
                    left_lane_inds = ret['left_lane_inds']
                    right_lane_inds = ret['right_lane_inds']

                    left_fit = self.left_line.add_fit(left_fit)
                    right_fit = self.right_line.add_fit(right_fit)

                    self.detected = True

            else:
                left_fit = self.left_line.get_fit()
                right_fit = self.right_line.get_fit()
                ret = tune_fit(img_birdeye, left_fit, right_fit)

                if ret is not None:
                    left_fit = ret['left_fit']
                    right_fit = ret['right_fit']
                    nonzerox = ret['nonzerox']
                    nonzeroy = ret['nonzeroy']
                    left_lane_inds = ret['left_lane_inds']
                    right_lane_inds = ret['right_lane_inds']

                    left_fit = self.left_line.add_fit(left_fit)
                    right_fit = self.right_line.add_fit(right_fit)

                else:
                    self.detected = False

            # Annotate original image
            bird_fit_img = None
            combine_fit_img = None
            if ret is not None:
                bird_fit_img = bird_fit(img_birdeye, ret, save_file=None)
                combine_fit_img = final_viz(img, left_fit, right_fit, Minv)
            else:
                print("Unable to detect lanes")

            return combine_fit_img, bird_fit_img


if __name__ == '__main__':
    # init args
    rospy.init_node('lanenet_node', anonymous=True)
    lanenet_detector()
    while not rospy.core.is_shutdown():
        rospy.rostime.wallsleep(0.5)
